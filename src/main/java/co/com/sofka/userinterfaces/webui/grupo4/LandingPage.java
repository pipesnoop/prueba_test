package co.com.sofka.userinterfaces.webui.grupo4;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

import static org.openqa.selenium.By.*;

public class LandingPage extends PageObject {

   public static final Target DESCRIPCION = Target
      .the("Descripcion ")
      .located(id("txt-descripcion"));

   public static final Target BOTON_REGISTRO = Target
      .the("registro")
      .located(id("btn-registro-home"));

   public static final Target BOTON_INICIO_SECCION = Target
      .the("Inicio de seccion")
      .located(id("btn-inicio-sesion-home"));

   public static final Target INFORMACION = Target
      .the("informacion")
      .located(id("txt-frase"));


}
