package co.com.sofka.userinterfaces.webui;

import net.serenitybdd.screenplay.targets.Target;

import static org.openqa.selenium.By.className;

public class MyStoreSingInLocator {
    public static final Target SIGN_IN = Target
            .the("Sign in")
            .located(className("header_user_info"));

}
