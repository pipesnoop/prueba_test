package co.com.sofka.model.ui.grupo4;

public class LandingPageModel {

   private String descripcion;
   private String botonRegistro;
   private String botonInicioDeSeccion;
   private String informacion;

   public String getDescripcion() {
      return descripcion;
   }

   public void setDescripcion(String descripcion) {
      this.descripcion = descripcion;
   }

   public String getBotonRegistro() {
      return botonRegistro;
   }

   public void setBotonRegistro(String botonRegistro) {
      this.botonRegistro = botonRegistro;
   }

   public String getBotonInicioDeSeccion() {
      return botonInicioDeSeccion;
   }

   public void setBotonInicioDeSeccion(String botonInicioDeSeccion) {
      this.botonInicioDeSeccion = botonInicioDeSeccion;
   }

   public String getInformacion() {
      return informacion;
   }

   public void setInformacion(String informacion) {
      this.informacion = informacion;
   }
}
