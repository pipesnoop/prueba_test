package co.com.sofka.question.services.rest.grupo4;

import co.com.sofka.model.services.rest.grupo4.UpgradeUserModel;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class UpgradeUserResponse implements Question<UpgradeUserModel> {

   @Override
   public UpgradeUserModel answeredBy (Actor actor){
      return SerenityRest.lastResponse().as(UpgradeUserModel.class);
   }

}
