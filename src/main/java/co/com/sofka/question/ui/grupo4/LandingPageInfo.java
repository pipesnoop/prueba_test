package co.com.sofka.question.ui.grupo4;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class LandingPageInfo implements Question<String> {

   public static final String MENSAJE_INFORMACION = "¡El parche, un lugar donde puedes crear eventos y parchar con " +
      "sofkianos con tus mismos gustos, te esperamos para tener momentos que deberian ser eternos!";

   @Override
   public String answeredBy(Actor actor) {
      return (MENSAJE_INFORMACION);
   }

   public static LandingPageInfo landinPageInfo(){
      return new LandingPageInfo();
   }


}
