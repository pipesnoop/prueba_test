package co.com.sofka.question.ui.grupo4;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class LandingPageInicioSession  implements Question<String> {

   public static final String MENSAJE_INICIO_SECCION= "INICIO DE SESIÓN";

   @Override
   public String answeredBy(Actor actor) {
      return MENSAJE_INICIO_SECCION;
   }

   public static LandingPageInicioSession landingPageInicioSession(){
      return new LandingPageInicioSession();
   }
}
