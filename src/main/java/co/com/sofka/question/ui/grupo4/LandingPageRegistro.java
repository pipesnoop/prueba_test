package co.com.sofka.question.ui.grupo4;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class LandingPageRegistro implements Question<String> {

   public static final String MENSAJE_REGISTRO = "REGISTRO";


   @Override
   public String answeredBy(Actor actor) {
      return MENSAJE_REGISTRO;
   }

   public static LandingPageRegistro landingPageRegistro(){
      return new LandingPageRegistro();
   }
}
