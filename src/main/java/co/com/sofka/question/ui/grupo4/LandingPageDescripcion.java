package co.com.sofka.question.ui.grupo4;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class LandingPageDescripcion implements Question<String> {

   public static final String MENSAJE_DESCRIPCION = "El proyecto permitirá el acceso a cualquier Sofkiano Colaborador; " +
      "como buscar un evento, indicar la asistencia y visualización de quienes van a participar, pertenecer a una o " +
      "varias comunidades donde se realicen eventos constantemente logrando enlazar la mayor cantidad de personas con " +
      "gustos similares, y con el tiempo filtrar cual será el próximo evento en la ubicación más cercana según donde " +
      "se encuentre el usuario.";

   @Override
   public String answeredBy(Actor actor) {
      return (MENSAJE_DESCRIPCION);
   }

   public static LandingPageDescripcion landingPageDescripcion(){
      return new LandingPageDescripcion();
   }
}
