package co.com.sofka.task.ui.grupo4;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.MoveMouse;

import static co.com.sofka.userinterfaces.webui.grupo4.LandingPage.BOTON_INICIO_SECCION;


public class LandingPageInicioSesionTask implements Task {
   @Override
   public <T extends Actor> void performAs(T actor) {
      actor.attemptsTo(
         MoveMouse.to(BOTON_INICIO_SECCION)
      );
   }

   public static LandingPageInicioSesionTask landingPageInicioSesionTask(){
      return new LandingPageInicioSesionTask();
   }

}
