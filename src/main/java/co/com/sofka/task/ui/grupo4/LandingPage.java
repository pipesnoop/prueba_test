package co.com.sofka.task.ui.grupo4;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.userinterfaces.webui.grupo4.LandingPage.*;

public class LandingPage implements Task {

   @Override
   public <T extends Actor> void performAs(T actor) {
      actor.attemptsTo(
         Scroll.to(INFORMACION)
      );

   }

   public static LandingPage landingPage(){
      return new LandingPage();
   }
}
