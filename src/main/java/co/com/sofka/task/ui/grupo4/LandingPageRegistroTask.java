package co.com.sofka.task.ui.grupo4;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.MoveMouse;

import static co.com.sofka.userinterfaces.webui.grupo4.LandingPage.*;

public class LandingPageRegistroTask implements Task {
   @Override
   public <T extends Actor> void performAs(T actor) {
      actor.attemptsTo(
         MoveMouse.to(BOTON_REGISTRO)
      );
   }

   public static LandingPageRegistroTask landingPageRegistroTask(){
      return new LandingPageRegistroTask();
   }
}
