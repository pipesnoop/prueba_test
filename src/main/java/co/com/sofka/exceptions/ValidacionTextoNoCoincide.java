package co.com.sofka.exceptions;

public class ValidacionTextoNoCoincide extends AssertionError{
   public static final String VALIDACION_NO_COINCIDE = "La validacion no coincide. %s";

   public ValidacionTextoNoCoincide(String message, Throwable cause){
      super(message, cause);
   }
}
