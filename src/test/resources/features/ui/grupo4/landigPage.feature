#language: es
Característica: landing Page
  Yo como usuario quiero ver en la página principal la información de la plataforma
  para tener una experiencia agradable al momento de ingresar.

  Antecedentes:
    Dado que el usuario está en la página Parches

  @LandinPageInfo
  Escenario: Verificar la Visualización de la información de la página
    Cuando el usuario realiza el proceso de inspección de la información
    Entonces el usuario deberá ver la información general de la página
    Y el usuario deberá ver la descripción

  @LandinPageRegisto
  Escenario: Verificar la existencia de el botón de registro
    Cuando el usuario quiere ver el botón registro
    Entonces el usuario deberá ver el botón Registrar

  @LandinPageInicioSession
  Escenario: Verificar la existencia de el botón de inicio de sesión
    Cuando el usuario quiere ver el botón inicio de sesión
    Entonces el usuario deberá ver el botón de Inicio de sesión