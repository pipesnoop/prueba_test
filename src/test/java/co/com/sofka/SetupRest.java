package co.com.sofka;


public class SetupRest extends GeneralSetupServices {
    protected static final String URL_BASE_PARCHE = "https://parche-qa.herokuapp.com";
    protected static final String RESOURCE_REGISTRO = "/crearUsuario";
    protected static final String RESOURCE_INICIO_SESION = "/inicioSesion/%s";

    protected static final String RESOURCE_ACTUALIZACION_USUARIO = "/actualizarUsuario";

    protected void setupRest() {
        actorCan(URL_BASE_PARCHE);
    }
}
