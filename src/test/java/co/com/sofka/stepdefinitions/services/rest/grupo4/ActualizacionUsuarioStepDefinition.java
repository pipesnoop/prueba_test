package co.com.sofka.stepdefinitions.services.rest.grupo4;

import co.com.sofka.SetupRest;
import co.com.sofka.model.services.rest.grupo4.UpgradeUserModel;
import co.com.sofka.question.services.rest.ResponseCode;
import co.com.sofka.question.services.rest.grupo4.UpgradeUserErrorResponse;
import co.com.sofka.question.services.rest.grupo4.UpgradeUserResponse;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;


import java.util.Locale;

import static co.com.sofka.task.services.rest.grupo4.DoPut.doPut;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.equalTo;


public class ActualizacionUsuarioStepDefinition extends SetupRest {
   public static Logger LOGGER = Logger.getLogger(ActualizacionUsuarioStepDefinition.class);
   private UpgradeUserModel upgradeUserModelOk;
   private UpgradeUserModel upgradeUserModelError;
   private final String mensajeError = "Not Found";

   Faker usFaker = new Faker(new Locale("es-MX"));


   @Dado("el usuario está registrado y se encuentra en la página de parches")
   public void elUsuarioEstaRegistradoYSeEncuentraEnLaPaginaHttpsParcheQaHerokuappCom() {
      try {
         super.setupRest();
         upgradeUser();
         upgradeUserError();
         LOGGER.info("Inicia proceso para las peticiones para los dos escenarios");
      } catch (Exception e) {
         LOGGER.warn(e.getMessage(), e);
         Assertions.fail("");
      }
   }

   @Cuando("el usuario envía la petición de actualizacion del usuario con los campos solicitados")
   public void elUsuarioEnviaLaPeticionDeActualizacionDelUsuarioConLosCamposSolicitados() {
      try {
         actor.attemptsTo(doPut()
            .withTheResource(RESOURCE_ACTUALIZACION_USUARIO)
            .andTheBodyRequest(upgradeUserModelOk)
         );
         LOGGER.info("Envio de la peticion para  el escenario 1");
      } catch (Exception e) {
         LOGGER.warn(e.getMessage(), e);
         Assertions.fail("");
      }
   }
   @Entonces("el usuario recibe un código de respuesta OK confirmando la actualizacion")
   public void elUsuarioRecibeUnCodigoDeRespuestaOKConfirmandoLaActualizacion() {
      try {
         actor.should(
            seeThat("El codigo de respuesta debe ser : " + HttpStatus.SC_OK, ResponseCode
               .was(), equalTo(HttpStatus.SC_OK))
         );
         LOGGER.info("Respuesta de la peticion 1 para  el escenario 1, status 200 ");
      } catch (Exception e) {
         LOGGER.warn(e.getMessage(), e);
         Assertions.fail("");
      }
   }
   @Y("el usuario podra ver su nombre actualizado")
   public void elUsuarioPodraVerSuNombreActualizado() {
      try {
         UpgradeUserModel actualResponse = new UpgradeUserResponse().answeredBy(actor);
         actor.should(
            seeThatResponse("el nombre de usuario debe ser :" + actualResponse.getNombres(),
               response -> response.body("nombres", equalTo(actualResponse.getNombres()))
               )
         );
         LOGGER.info("Respuesta de la peticion 2 para  el escenario 1, nombre usuario ");
      }catch (Exception e){
         LOGGER.warn(e.getMessage(), e);
         Assertions.fail("");
      }
   }

   @Cuando("el usuario envía la petición de actualizacion del usuario con id inexistente")
   public void elUsuarioEnviaLaPeticionDeActualizacionDelUsuarioConIdInexistente() {
      try {
         actor.attemptsTo(doPut()
            .withTheResource(RESOURCE_ACTUALIZACION_USUARIO)
            .andTheBodyRequest(upgradeUserModelError)
         );
         LOGGER.info("Envio de la peticion para  el escenario 2");
      } catch (Exception e) {
         LOGGER.warn(e.getMessage(), e);
         Assertions.fail("");
      }
   }

   @Entonces("el usuario recibe un código de respuesta Not Found")
   public void elUsuarioRecibeUnCodigoDeRespuestaNotFound() {
      try {
         actor.should(
            seeThat("El codigo de respuesta debe ser : " + HttpStatus.SC_NOT_FOUND, ResponseCode
               .was(), equalTo(HttpStatus.SC_NOT_FOUND)),
            seeThat("El error generado debe ser 'Not Found'",
               datos -> new UpgradeUserErrorResponse().answeredBy(actor).getError(),equalTo(mensajeError))
         );
         LOGGER.info("Respuesta de la peticion  para  el escenario 2, Not Found ");
      } catch (Exception e) {
         LOGGER.warn(e.getMessage(), e);
         Assertions.fail("");
      }
   }

   private UpgradeUserModel upgradeUser(){
      String nombres = usFaker.name().fullName();

      upgradeUserModelOk = new UpgradeUserModel();
      upgradeUserModelOk.setId("61fa07fe663a6d1daf18e9cc");
      upgradeUserModelOk.setUid("M1PPeC5Ax5flEqSRMyzKpXAgOGD2");
      upgradeUserModelOk.setNombres(nombres);
      upgradeUserModelOk.setEmail("josemejia1425@gmail.com");
      upgradeUserModelOk.setImagenUrl("imagen");
      return upgradeUserModelOk;
   }

   private UpgradeUserModel upgradeUserError(){
      upgradeUserModelError = new UpgradeUserModel();
      upgradeUserModelError.setId("61fa07fe663a6d1daf18e9");
      upgradeUserModelError.setUid("M1PPeC5Ax5flEqSRMyzKpXAgOGD2");
      upgradeUserModelError.setNombres("Jose Mejia");
      upgradeUserModelError.setEmail("josemejia1425@gmail.com");
      upgradeUserModelError.setImagenUrl("imagen");
      return upgradeUserModelError;
   }


}
