package co.com.sofka.stepdefinitions.ui.grupo4;

import co.com.sofka.GeneralSetupUi;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;



import static co.com.sofka.question.ui.grupo4.LandingPageDescripcion.landingPageDescripcion;
import static co.com.sofka.question.ui.grupo4.LandingPageInfo.landinPageInfo;
import static co.com.sofka.question.ui.grupo4.LandingPageInicioSession.landingPageInicioSession;
import static co.com.sofka.question.ui.grupo4.LandingPageRegistro.landingPageRegistro;
import static co.com.sofka.task.ui.grupo4.LandingPage.landingPage;
import static co.com.sofka.task.ui.grupo4.LandingPageInicioSesionTask.landingPageInicioSesionTask;
import static co.com.sofka.task.ui.grupo4.LandingPageRegistroTask.landingPageRegistroTask;
import static co.com.sofka.task.ui.grupo4.OpenLandingPage.openLandingPage;
import static co.com.sofka.userinterfaces.webui.grupo4.LandingPage.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class landingpage extends GeneralSetupUi {

   public static Logger LOGGER = Logger.getLogger(landingpage.class);

   private static final String ACTOR_NAME = "Ivan";


   @Dado("que el usuario está en la página Parches")
   public void queElUsuarioEstaEnLaPaginaParches() {
      try {
         actorSetupTheBrowser(ACTOR_NAME);
         theActorInTheSpotlight().wasAbleTo(
            openLandingPage()
         );
         LOGGER.info("** Inicio scenario");
      }catch (Exception e){
         LOGGER.error(e);
         LOGGER.info("** Error en el inicio del escenario");
         Assertions.fail("");
      }
   }

   @Cuando("el usuario realiza el proceso de inspección de la información")
   public void elUsuarioRealizaElProcesoDeInspeccionDeLaInformacion() {
      try {
         theActorInTheSpotlight().attemptsTo(
            landingPage()
         );
         LOGGER.info("** Inicio Generacion proceso escenario 1");
      }catch (Exception e){
         LOGGER.error(e);
         LOGGER.info("Error en el inicio del la generacion del proceso del escenario 1");
         Assertions.fail("");
      }
   }

   @Entonces("el usuario deberá ver la información general de la página")
   public void elUsuarioDeberaVerLaInformacionGeneralDeLaPagina() {
      try {
         theActorInTheSpotlight().should(
            seeThat(
               landinPageInfo(),equalTo(INFORMACION.resolveFor(theActorInTheSpotlight()).getText())
            )
         );
         LOGGER.info("** Inicio verificacion proceso 1 scenario 1");
      }catch (Exception e){
         LOGGER.error(e);
         LOGGER.info("Error en el inicio del la verificacion del proceso del escenario 1");
         Assertions.fail("");
      }
   }

   @Entonces("el usuario deberá ver la descripción")
   public void elUsuarioDeberaVerLaDescripcion() {
      try {
         theActorInTheSpotlight().should(
            seeThat(
               landingPageDescripcion(),equalTo(DESCRIPCION.resolveFor(theActorInTheSpotlight()).getText())
            )
         );
         LOGGER.info("** Inicio verificacion proceso 2 scenario 1");
      }catch (Exception e){
         LOGGER.error(e);
         LOGGER.info("Error en el inicio del la verificacion del proceso 2 del escenario 1");
         Assertions.fail("");
      }
   }

   @Cuando("el usuario quiere ver el botón registro")
   public void elUsuarioQuiereVerElBotonRegistro() {
      try {
         theActorInTheSpotlight().attemptsTo(
            landingPage(),
            landingPageRegistroTask()
         );
         LOGGER.info("** Inicio Generacion proceso escenario 2");
      }catch (Exception e){
         LOGGER.error(e);
         LOGGER.info("Error en el inicio del la generacion del proceso del escenario 2");
         Assertions.fail("");
      }
   }

   @Entonces("el usuario deberá ver el botón Registrar")
   public void elUsuarioDeberaVerElBotonRegistrar() {
      try {
         theActorInTheSpotlight().should(
            seeThat(
               landingPageRegistro(), equalTo(BOTON_REGISTRO.resolveFor(theActorInTheSpotlight()).getText())
            )
         );
         LOGGER.info("** Inicio verificacion proceso 2 scenario 2");
      }catch (Exception e){
         LOGGER.error(e);
         LOGGER.info("Error en el inicio del la verificacion del proceso 2 del escenario 2");
         Assertions.fail("");
      }
   }

   @Cuando("el usuario quiere ver el botón inicio de sesión")
   public void elUsuarioQuiereVerElBotonInicioDeSesion() {
      try {
         theActorInTheSpotlight().attemptsTo(
            landingPage(),
            landingPageInicioSesionTask()
         );
         LOGGER.info("** Inicio Generacion proceso escenario 3");
      }catch (Exception e){
         LOGGER.error(e);
         LOGGER.info("Error en el inicio del la generacion del proceso del escenario 3");
         Assertions.fail("");
      }
   }

   @Entonces("el usuario deberá ver el botón de Inicio de sesión")
   public void elUsuarioDeberaVerElBotonDeInicioDeSesion() {
      try {
         theActorInTheSpotlight().should(
            seeThat(
               landingPageInicioSession(), equalTo(BOTON_INICIO_SECCION.resolveFor(theActorInTheSpotlight()).getText())
            )
         );
         LOGGER.info("** Inicio verificacion proceso  scenario 3");
      }catch (Exception e){
         LOGGER.error(e);
         LOGGER.info("Error en el inicio del la verificacion del proceso  del escenario 3");
         Assertions.fail("");
      }
   }



}
