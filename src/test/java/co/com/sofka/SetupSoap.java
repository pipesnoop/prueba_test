package co.com.sofka;

public class SetupSoap extends GeneralSetupServices {
    protected static final String URL_BASE = "http://webservices.oorsprong.org";
    protected static final String RESOURCE = "/websamples.countryinfo/CountryInfoService.wso";

    protected void setupWebSamples () {
        actorCan(URL_BASE);
    }

}
